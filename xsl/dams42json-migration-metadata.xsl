<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
      xmlns:dams="http://library.ucsd.edu/ontology/dams#"
      xmlns:mads="http://www.loc.gov/mads/rdf/v1#"
      xmlns:owl="http://www.w3.org/2002/07/owl#"
      xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
      exclude-result-prefixes="dams mads owl rdf rdfs">

    <xsl:template name="damsElements">
        <xsl:param name="modelName"/>

        <!-- section title: mads:Title -->
        <xsl:for-each select="*[local-name()='title']/mads:Title">
            <xsl:call-template name="madsTitle">
                <xsl:with-param name="modelName"><xsl:value-of select="$modelName"/></xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>

        <!-- section language: mads:Language -->
        <xsl:for-each select="*[local-name()='language']">
           <xsl:variable name='resArk'><xsl:value-of select="*/@rdf:about | @rdf:resource"/></xsl:variable>

           <xsl:call-template name="madsLanguage">
               <xsl:with-param name="node" select="//*[@rdf:about=$resArk] | *" />
               <xsl:with-param name="modelName"><xsl:value-of select="$modelName"/></xsl:with-param>
               <xsl:with-param name="ark"><xsl:value-of select="$resArk"/></xsl:with-param>
           </xsl:call-template>
        </xsl:for-each>

        <!-- section contributions: dams:Relationship -->
        <xsl:for-each select="*[local-name()='relationship']/dams:Relationship">
            <xsl:call-template name="damsRelationship">
                <xsl:with-param name="modelName"><xsl:value-of select="$modelName"/></xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>

        <!-- section dates: dams:Date -->
        <xsl:for-each select="*[local-name()='date']/dams:Date">
            <xsl:call-template name="damsDate">
                <xsl:with-param name="modelName"><xsl:value-of select="$modelName"/></xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>

        <!-- section types: dams:typeOfResource -->
        <xsl:for-each select="*[local-name()='typeOfResource']/text()">
           <xsl:call-template name="appendJsonObject">
               <xsl:with-param name="key">type_of_resource</xsl:with-param>
               <xsl:with-param name="val"><xsl:value-of select="."/></xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>

        <!-- section notes: dams:Note[not(dams:type='identifier')] -->
        <xsl:for-each select="*[local-name()='note']/@rdf:resource | *[local-name()='note']/dams:Note[not(dams:type='identifier')]">
            <xsl:sort select="*/dams:type"/>
            <xsl:variable name='resArk'><xsl:value-of select="*/@rdf:about | @rdf:resource"/></xsl:variable>
            <xsl:choose>
                <xsl:when test="$resArk != ''">
                    <xsl:call-template name="damsNote" select="//*[@rdf:about=$resArk]">
                        <xsl:with-param name="modelName"><xsl:value-of select="$modelName"/></xsl:with-param>
                    </xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="damsNote">
                        <xsl:with-param name="modelName"><xsl:value-of select="$modelName"/></xsl:with-param>
                    </xsl:call-template>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>

        <!-- section identifiers: dams:Note[type='dams:identifier'] -->
        <xsl:for-each select="*[local-name()='note']/dams:Note[dams:type='identifier']">
            <xsl:call-template name="damsNoteIdentifier"/>
        </xsl:for-each>

        <!-- section geographics: dams:Cartographics -->
        <xsl:for-each select="*[local-name()='cartographics']/@rdf:resource | *[local-name()='cartographics']/dams:Cartographics">
           <xsl:variable name='resArk'><xsl:value-of select="*/@rdf:about | @rdf:resource"/></xsl:variable>
           <xsl:choose>
             <xsl:when test="$resArk != ''">
                 <xsl:call-template name="damsCartographics" select="//*[@rdf:about=$resArk]" />
             </xsl:when>
             <xsl:otherwise>
                 <xsl:call-template name="damsCartographics" />
             </xsl:otherwise>
           </xsl:choose>
        </xsl:for-each>

        <!-- section subject: mads:Subject | dams:Subject -->
        <xsl:for-each select="*[local-name() = 'conferenceName' or local-name() = 'corporateName' or local-name() = 'familyName' or local-name() = 'genreForm' or local-name() = 'geographic' or local-name() = 'occupation' or local-name() = 'personalName' or local-name() = 'temporal' or local-name() = 'topic' or local-name() = 'anatomy' or local-name() = 'commonName' or local-name() = 'cruise' or local-name() = 'culturalContext' or local-name() = 'lithology' or local-name() = 'scientificName' or local-name() = 'series']">
            <xsl:sort select="name()" order="descending"/>
            <xsl:variable name='resArk'><xsl:value-of select="*/@rdf:about | @rdf:resource"/></xsl:variable>
           <xsl:choose>
             <xsl:when test="$resArk != ''">
                 <xsl:call-template name="subjectElement">
                     <xsl:with-param name="node" select="//*[@rdf:about=$resArk]" />
                 </xsl:call-template>
             </xsl:when>
             <xsl:otherwise>
                 <xsl:call-template name="subjectElement">
                     <xsl:with-param name="node" select="*" />
                 </xsl:call-template>
             </xsl:otherwise>
           </xsl:choose>
        </xsl:for-each>

        <!-- section rights: dams:License -->
        <xsl:for-each select="*[local-name()='license']/@rdf:resource | *[local-name()='license']/dams:License">
            <xsl:variable name='resArk'><xsl:value-of select="*/@rdf:about | @rdf:resource"/></xsl:variable>
            <xsl:choose>
                <xsl:when test="$resArk != ''">
                    <xsl:call-template name="damsLicense" select="//*[@rdf:about=$resArk]" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="damsLicense" />
                </xsl:otherwise>
              </xsl:choose>
        </xsl:for-each>

        <!-- section rights: dams:Copyright -->
        <xsl:for-each select="*[local-name()='copyright']/@rdf:resource | *[local-name()='copyright']/dams:Copyright">
            <xsl:variable name='resArk'><xsl:value-of select="*/@rdf:about | @rdf:resource"/></xsl:variable>
            <xsl:choose>
                <xsl:when test="$resArk != ''">
                    <xsl:call-template name="damsCopyright" select="//*[@rdf:about=$resArk]" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:call-template name="damsCopyright" />
                </xsl:otherwise>
              </xsl:choose>
        </xsl:for-each>

        <!-- Collection(s) -->
        <xsl:if test="*[contains(local-name(), 'Collection')]">
            <xsl:for-each select="*[contains(local-name(), 'Collection') and not(contains(local-name(), 'has'))]">
                <xsl:sort select="*/@rdf:about | @rdf:resource"/>
                <xsl:variable name='resArk'><xsl:value-of select="*/@rdf:about | @rdf:resource"/></xsl:variable>
                <xsl:variable name="ark"><xsl:value-of select="substring-after($resArk, '/20775/')" /></xsl:variable>

                <xsl:call-template name="appendJsonObject">
                    <xsl:with-param name="key">parents</xsl:with-param>
                    <xsl:with-param name="val"><xsl:value-of select="$ark"/></xsl:with-param>
                </xsl:call-template>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>

    <!-- section: title -->
    <xsl:template name="madsTitle" match="mads:Title">
        <xsl:param name="modelName"/>

        <xsl:for-each select="mads:elementList">
            <xsl:for-each select="*[local-name() != 'NonSortElement']">
                <xsl:variable name="titleName">
                    <xsl:choose>
                        <xsl:when test="local-name()='MainTitleElement'">title</xsl:when>
                        <xsl:when test="local-name()='SubTitleElement'">title_subtitle</xsl:when>
                        <xsl:when test="local-name()='PartNameElement'">title_part_name</xsl:when>
                        <xsl:when test="local-name()='PartNumberElement'">title_part_number</xsl:when>
                        <xsl:otherwise><xsl:value-of select="concat('title_unknown:', local-name())"/></xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>

                <xsl:if test="$titleName != ''">
                    <xsl:call-template name="appendJsonObject">
                        <xsl:with-param name="key"><xsl:value-of select="$titleName"/></xsl:with-param>
                        <xsl:with-param name="val">
                            <xsl:choose>
                                <xsl:when test="../mads:NonSortElement"><xsl:value-of select="concat(../mads:NonSortElement,' ',mads:elementValue)"/></xsl:when>
                                <xsl:otherwise><xsl:value-of select="mads:elementValue"/></xsl:otherwise>
                            </xsl:choose>
                        </xsl:with-param>
                    </xsl:call-template>
                </xsl:if>
            </xsl:for-each>
        </xsl:for-each>
        <xsl:for-each select="*[contains(local-name(), 'Variant')]">
            <xsl:call-template name="appendJsonObject">
                <xsl:with-param name="key">
                    <xsl:choose>
                        <xsl:when test="local-name()='hasTranslationVariant'">title_translation</xsl:when>
                        <xsl:when test="$modelName = 'GeospatialObject'">title_alternative_geo</xsl:when>
                        <xsl:otherwise>title_alternative</xsl:otherwise>
                    </xsl:choose>
                </xsl:with-param>
                <xsl:with-param name="val"><xsl:value-of select="mads:Variant/mads:variantLabel"/></xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:template>

    <!-- section language: mads:Language -->
    <xsl:template name="madsLanguage" match="mads:Language">
        <xsl:param name="node"/>
        <xsl:param name="modelName"/>
        <xsl:param name="ark"/>

        <xsl:variable name="resArk">
            <xsl:choose>
                <xsl:when test="string-length($ark) > 0"><xsl:value-of select="concat('@', $ark)"/></xsl:when>
                <xsl:otherwise></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="language_key">
            <xsl:choose>
                <xsl:when test="$modelName = 'GeospatialObject'">language_geo</xsl:when>
                <xsl:otherwise>language</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="code"><xsl:value-of select="$node/mads:code"/></xsl:variable>
        <xsl:variable name="isoUrl">
            <xsl:choose>
                <xsl:when test="$code = 'ase'">http://id.loc.gov/vocabulary/iso639-2/sgn</xsl:when>
                <xsl:when test="string-length($code) = 3"><xsl:value-of select="concat('http://id.loc.gov/vocabulary/iso639-2/', $code)"/></xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="concat('language_unknown:', $code, '-', $node/mads:authoritativeLabel, $resArk)"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:call-template name="appendJsonObject">
           <xsl:with-param name="key"><xsl:value-of select="$language_key"/></xsl:with-param>
           <xsl:with-param name="val"><xsl:value-of select="$isoUrl"/></xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <!-- section: contributors -->
    <xsl:template name="damsRelationship" match="dams:Relationship">
        <xsl:param name="modelName"/>

        <xsl:variable name="roleArk" select="dams:role//@rdf:about | dams:role/@rdf:resource" />
        <xsl:variable name="nameArk" select="*[contains(local-name(), 'Name') or contains(local-name(), 'name')]//@rdf:about | *[contains(local-name(), 'Name') or contains(local-name(), 'name')]/@rdf:resource" />
        <xsl:variable name="ark"><xsl:value-of select="substring-after($nameArk, '/20775/')" /></xsl:variable>
        <xsl:variable name="role">
          <xsl:choose>
              <xsl:when test="//mads:Authority[@rdf:about=$roleArk]/mads:authoritativeLabel | //mads:Authority[@rdf:about=$roleArk]/rdf:value">
                  <xsl:value-of select="translate(//mads:Authority[@rdf:about=$roleArk]/mads:authoritativeLabel | //mads:Authority[@rdf:about=$roleArk]/rdf:value, $upper, $lower)"/>
              </xsl:when>
              <xsl:when test="//*[@rdf:about=$roleArk]/mads:authoritativeLabel | //*[@rdf:about=$roleArk]/rdf:value">
                  <xsl:value-of select="concat(//*[@rdf:about=$roleArk]/mads:authoritativeLabel | //*[@rdf:about=$roleArk]/rdf:value, 'Corrupted')"/>
              </xsl:when>
              <xsl:otherwise>
                  <xsl:value-of select="substring-after($roleArk, '/20775/')"/>
              </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>

        <xsl:variable name="name">
            <xsl:choose>
                <xsl:when test="*[contains(local-name(), 'personal')] and //mads:PersonalName[@rdf:about=$nameArk]">person</xsl:when>
                <xsl:when test="*[contains(local-name(), 'corporate')] and //mads:CorporateName[@rdf:about=$nameArk]">corporate</xsl:when>
                <xsl:when test="*[local-name()='name'] and //mads:Name[@rdf:about=$nameArk]">name</xsl:when>
                <xsl:when test="*[contains(local-name(), 'personal')]">personCorrupted</xsl:when>
                <xsl:when test="*[contains(local-name(), 'corporate')]">corporateCorrupted</xsl:when>
                <xsl:when test="*[local-name()='name']">nameCorrupted</xsl:when>
                <xsl:otherwise>nameUnknown</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:variable name="agent_key">
            <xsl:choose>
                <xsl:when test="$name != 'person' and $name != 'corporate' and $name != 'name'"><xsl:value-of select="concat('agent_corrupted:', $role, '_', $name)"/></xsl:when>
                <xsl:when test="$modelName = 'GeospatialObject' and $role = 'creator'">creator_geo</xsl:when>
                <xsl:when test="$role = 'creator'">agent_creator</xsl:when>
                <xsl:when test="$role = 'principal investigator'">agent_principal_investigator</xsl:when>
                <xsl:when test="$role = 'author'">agent_author</xsl:when>
                <xsl:when test="$role = 'photographer'">agent_photographer</xsl:when>
                <xsl:when test="$role = 'contributor'">agent_contributor</xsl:when>
                <xsl:when test="$role = 'publisher'">agent_publisher</xsl:when>
                <xsl:when test="$role = 'broadcaster'">agent_broadcaster</xsl:when>
                <xsl:when test="$role = 'laboratory'">agent_laboratory</xsl:when>
                <xsl:when test="$role = 'repository'">agent_repository</xsl:when>
                <xsl:when test="$role = 'vessel'">agent_vessel</xsl:when>
                <xsl:when test="$role = 'actor'">agent_actor</xsl:when>
                <xsl:when test="$role = 'composer'">agent_composer</xsl:when>
                <xsl:when test="$role = 'conductor'">agent_conductor</xsl:when>
                <xsl:when test="$role = 'co-principal investigator'">agent_co_principal_investigator</xsl:when>
                <xsl:when test="$role = 'correspondent'">agent_correspondent</xsl:when>
                <xsl:when test="$role = 'curator'">agent_curator</xsl:when>
                <xsl:when test="$role = 'director'">agent_director</xsl:when>
                <xsl:when test="$role = 'dissertant'">agent_dissertant</xsl:when>
                <xsl:when test="$role = 'interviewee'">agent_interviewee</xsl:when>
                <xsl:when test="$role = 'interviewer'">agent_interviewer</xsl:when>
                <xsl:when test="$role = 'moderator'">agent_moderator</xsl:when>
                <xsl:when test="$role = 'musician'">agent_musician</xsl:when>
                <xsl:when test="$role = 'painter'">agent_painter</xsl:when>
                <xsl:when test="$role = 'panelist'">agent_panelist</xsl:when>
                <xsl:when test="$role = 'research team head'">agent_research_team_head</xsl:when>
                <xsl:when test="$role = 'research team member'">agent_research_team_member</xsl:when>
                <xsl:when test="$role = 'researcher'">agent_researcher</xsl:when>
                <xsl:when test="$role = 'sculptor'">agent_sculptor</xsl:when>
                <xsl:when test="$role = 'singer'">agent_singer</xsl:when>
                <xsl:when test="$role = 'speaker'">agent_speaker</xsl:when>
                <xsl:when test="$role = 'architect'">agent_architect</xsl:when>
                <xsl:when test="$role = 'artist'">agent_artist</xsl:when>
                <xsl:when test="$role = 'collector'">agent_collector</xsl:when>
                <xsl:when test="$role = 'data manager'">agent_data_manager</xsl:when>
                <xsl:when test="$role = 'donor'">agent_donor</xsl:when>
                <xsl:when test="$role = 'editor'">agent_editor</xsl:when>
                <xsl:when test="$role = 'filmmaker'">agent_filmmaker</xsl:when>
                <xsl:when test="$role = 'former owner'">agent_former_owner</xsl:when>
                <xsl:when test="$role = 'host'">agent_host</xsl:when>
                <xsl:when test="$role = 'organizer'">agent_organizer</xsl:when>
                <xsl:when test="$role = 'performer'">agent_performer</xsl:when>
                <xsl:when test="$role = 'producer'">agent_producer</xsl:when>
                <xsl:when test="$role = 'recording engineer'">agent_recording_engineer</xsl:when>
                <xsl:when test="$role = 'recordist'">agent_recordist</xsl:when>
                <xsl:when test="$role = 'sponsor'">agent_sponsor</xsl:when>
                <xsl:when test="$role = 'analyst'">agent_analyst</xsl:when>
                <xsl:when test="$role = 'arranger'">agent_arranger</xsl:when>
                <xsl:when test="$role = 'cinematographer'">agent_cinematographer</xsl:when>
                <xsl:when test="$role = 'compiler'">agent_compiler</xsl:when>
                <xsl:when test="$role = 'data contributor'">agent_data_contributor</xsl:when>
                <xsl:when test="$role = 'dedicatee'">agent_dedicatee</xsl:when>
                <xsl:when test="$role = 'designer'">agent_designer</xsl:when>
                <xsl:when test="$role = 'distributor'">agent_distributor</xsl:when>
                <xsl:when test="$role = 'draftsman'">agent_draftsman</xsl:when>
                <xsl:when test="$role = 'enacting jurisdiction'">agent_enacting_jurisdiction</xsl:when>
                <xsl:when test="$role = 'field director'">agent_field_director</xsl:when>
                <xsl:when test="$role = 'film director'">agent_film_director</xsl:when>
                <xsl:when test="$role = 'film editor'">agent_film_editor</xsl:when>
                <xsl:when test="$role = 'film producer'">agent_film_producer</xsl:when>
                <xsl:when test="$role = 'illustrator'">agent_illustrator</xsl:when>
                <xsl:when test="$role = 'issuing body'">agent_issuing_body</xsl:when>
                <xsl:when test="$role = 'landscape architect'">agent_landscape_architect</xsl:when>
                <xsl:when test="$role = 'narrator'">agent_narrator</xsl:when>
                <xsl:when test="$role = 'permitting agency'">agent_permitting_agency</xsl:when>
                <xsl:when test="$role = 'presenter'">agent_presenter</xsl:when>
                <xsl:when test="$role = 'printer'">agent_printer</xsl:when>
                <xsl:when test="$role = 'printer'">agent_printer</xsl:when>
                <xsl:when test="$role = 'production company'">agent_production_company</xsl:when>
                <xsl:when test="$role = 'programmer'">agent_programmer</xsl:when>
                <xsl:when test="$role = 'project director'">agent_project_director</xsl:when>
                <xsl:when test="$role = 'screenwriter'">agent_screenwriter</xsl:when>
                <xsl:when test="$role = 'set designer'">agent_set_designer</xsl:when>
                <xsl:when test="$role = 'signer'">agent_signer</xsl:when>
                <xsl:when test="$role = 'technical director'">agent_technical_director</xsl:when>
                <xsl:when test="$role = 'thesis advisor'">agent_thesis_advisor</xsl:when>
                <xsl:when test="$role = 'transcriber'">agent_transcriber</xsl:when>
                <xsl:when test="$role = 'translator'">agent_translator</xsl:when>
                <xsl:when test="$role = 'videographer'">agent_videographer</xsl:when>
                <xsl:otherwise><xsl:value-of select="concat('agent_unknown:', $role, '_', $name)"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:if test="$agent_key != ''">
            <xsl:call-template name="appendJsonObject">
                <xsl:with-param name="key"><xsl:value-of select="$agent_key"/></xsl:with-param>
                <xsl:with-param name="val">
                    <xsl:value-of select="//*[@rdf:about=$nameArk]/mads:authoritativeLabel"/>
                </xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <!-- section: dates -->
    <xsl:template name="damsDate" match="dams:Date">
        <xsl:param name="modelName"/>

        <xsl:variable name="type">
            <xsl:choose>
                <xsl:when test="string-length(dams:type) > 0"><xsl:value-of select="dams:type"/></xsl:when>
                <xsl:otherwise>creation</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:if test="rdf:value">
            <xsl:variable name="date_key">
                <xsl:choose>
                    <xsl:when test="$type = 'collected'">date_collection</xsl:when>
                    <xsl:when test="$type = 'creation'">date_created</xsl:when>
                    <xsl:when test="$modelName = 'RdcpObject' and $type = 'issued'">date_issued_rdcp</xsl:when>
                    <xsl:when test="$type = 'issued'">date_issued</xsl:when>
                    <xsl:when test="$type = 'event'">date_event</xsl:when>
                    <xsl:when test="$type = 'copyright'">date_copyrighted</xsl:when>
                    <xsl:otherwise>date_unknown:<xsl:value-of select="$type"/>_<xsl:value-of select="rdf:value"/></xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:call-template name="appendJsonObject">
               <xsl:with-param name="key"><xsl:value-of select="$date_key"/></xsl:with-param>
               <xsl:with-param name="val"><xsl:value-of select="rdf:value"/></xsl:with-param>
            </xsl:call-template>
        </xsl:if>
        <xsl:if test="dams:beginDate">
            <xsl:call-template name="appendJsonObject">
                <xsl:with-param name="key">
                    <xsl:choose>
                        <xsl:when test="$modelName = 'GeospatialObject'">begin_geo</xsl:when>
                        <xsl:otherwise>begin</xsl:otherwise>
                    </xsl:choose>
                </xsl:with-param>
                <xsl:with-param name="val"><xsl:value-of select="dams:beginDate"/></xsl:with-param>
            </xsl:call-template>
        </xsl:if>
        <xsl:if test="dams:endDate">
            <xsl:call-template name="appendJsonObject">
               <xsl:with-param name="key">end</xsl:with-param>
               <xsl:with-param name="val"><xsl:value-of select="dams:endDate"/></xsl:with-param>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <!-- section: notes -->
    <xsl:template name="damsNote" match="dams:Note[not(dams:type='identifier')]">
        <xsl:param name="modelName"/>

        <xsl:variable name="type"><xsl:value-of select="dams:type"/></xsl:variable>
        <xsl:variable name="displayLabel"><xsl:value-of select="dams:displayLabel"/></xsl:variable>
        <xsl:variable name="note_key">
            <xsl:choose>
                <xsl:when test="$type = 'description'">note_description</xsl:when>
                <xsl:when test="$type = 'inscription'">note_inscription</xsl:when>
                <xsl:when test="$type = 'location of originals'">note_location_of_originals</xsl:when>
                <xsl:when test="$modelName = 'RdcpObject' and $type = 'preferred citation'">note_preferred_citation_rdcp</xsl:when>
                <xsl:when test="$type = 'preferred citation'">note_preferred_citation</xsl:when>
                <xsl:when test="$type = 'scope and content' or $displayLabel = 'scope and content'">note_scope_and_content</xsl:when>
                <xsl:when test="$type = 'physical description'">note_physical_description</xsl:when>
                <xsl:when test="$modelName = 'RdcpObject' and $type = 'technical details'">note_technical_details_rdcp</xsl:when>
                <xsl:when test="$type = 'technical details'">note_technical_details</xsl:when>
                <xsl:when test="$type = 'funding'">note_funding</xsl:when>
                <xsl:when test="$type = 'arrangement'">note_arrangement</xsl:when>
                <xsl:when test="$type = 'note' or $type = ''">note_general</xsl:when>
                <xsl:when test="$type = 'table of contents'">note_table_of_contents</xsl:when>
                <xsl:when test="$type = 'digital origin'">note_digital_origin</xsl:when>
                <xsl:when test="$type = 'classification'">note_classification</xsl:when>
                <xsl:when test="$type = 'extent'">note_extent</xsl:when>
                <xsl:when test="$type = 'credits'">note_credits</xsl:when>
                <xsl:when test="$type = 'abstract'">note_abstract</xsl:when>
                <xsl:when test="$type = 'bibliography'">note_bibliography</xsl:when>
                <xsl:when test="$type = 'biography'">note_biography</xsl:when>
                <xsl:when test="$type = 'custodial history'">note_custodial_history</xsl:when>
                <xsl:when test="$type = 'edition'">note_edition</xsl:when>
                <xsl:when test="$type = 'local attribution'">note_local_attribution</xsl:when>
                <xsl:when test="$type = 'methods'">note_methods</xsl:when>
                <xsl:when test="$type = 'material details'">note_material_details</xsl:when>
                <xsl:when test="$type = 'material details:finds'">note_material_details_finds</xsl:when>
                <xsl:when test="$type = 'material details:limits'">note_material_details_limits</xsl:when>
                <xsl:when test="$type = 'material details:relationship to other loci'">note_material_details_relationship_to_other_loci</xsl:when>
                <xsl:when test="$type = 'material details:storage method'">note_material_details_storage_method</xsl:when>
                <xsl:when test="$type = 'material details:water depth'">note_material_details_water_depth</xsl:when>
                <xsl:when test="$type = 'performers'">note_performers</xsl:when>
                <xsl:when test="$type = 'publication'">note_publication</xsl:when>
                <xsl:when test="$type = 'related publications'">note_related_publications</xsl:when>
                <xsl:when test="$type = 'series'">note_series</xsl:when>
                <xsl:when test="$type = 'statement of responsibility'">note_statement_of_responsibility</xsl:when>
                <xsl:when test="$type = 'venue'">note_venue</xsl:when>
                <xsl:when test="$type = 'work featured'">note_work_featured</xsl:when>
                <xsl:otherwise>note_unknown:<xsl:value-of select="$type"/>_<xsl:value-of select="$displayLabel"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:call-template name="appendJsonObject">
           <xsl:with-param name="key"><xsl:value-of select="$note_key"/></xsl:with-param>
           <xsl:with-param name="val"><xsl:value-of select="rdf:value"/></xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <!-- section: identifiers -->
    <xsl:template name="damsNoteIdentifier" match="dams:Note[dams:type='identifier']" priority="1">
        <xsl:variable name="displayLabel"><xsl:value-of select="dams:displayLabel"/></xsl:variable>
        <xsl:variable name="identifier_key">
            <xsl:choose>
                <xsl:when test="$displayLabel = 'ARK'">identifier_ark</xsl:when>
                <xsl:when test="$displayLabel = 'doi'">identifier_doi</xsl:when>
                <xsl:when test="$displayLabel = 'filename'">identifier_file_name</xsl:when>
                <xsl:when test="$displayLabel = 'ORCID'">identifier_orcid</xsl:when>
                <xsl:when test="$displayLabel = 'identifier'">identifier</xsl:when>
                <xsl:when test="$displayLabel = 'accession number'">identifier_accession</xsl:when>
                <xsl:when test="$displayLabel = 'basket number'">identifier_basket_number</xsl:when>
                <xsl:when test="$displayLabel = 'call number'">identifier_call_number</xsl:when>
                <xsl:when test="$displayLabel = 'mms_id'">identifier_mms</xsl:when>
                <xsl:when test="$displayLabel = 'collection number'">identifier_collection_number</xsl:when>
                <xsl:when test="$displayLabel = 'edm'">identifier_edm</xsl:when>
                <xsl:when test="$displayLabel = 'event id'">identifier_event_number</xsl:when>
                <xsl:when test="$displayLabel = 'igsn'">identifier_igsn</xsl:when>
                <xsl:when test="$displayLabel = 'local'">identifier_local</xsl:when>
                <xsl:when test="$displayLabel = 'locus number'">identifier_locus_number</xsl:when>
                <xsl:when test="$displayLabel = 'negative'">identifier_negative</xsl:when>
                <xsl:when test="$displayLabel = 'news release number'">identifier_news_release_number</xsl:when>
                <xsl:when test="$displayLabel = 'oclc number'">identifier_oclc_number</xsl:when>
                <xsl:when test="$displayLabel = 'registration number'">identifier_registration_number</xsl:when>
                <xsl:when test="$displayLabel = 'samplenumber '">identifier_sample_number</xsl:when>
                <xsl:when test="$displayLabel = 'sequence'">identifier_sequence</xsl:when>
                <xsl:when test="$displayLabel = 'shared shelf'">identifier_jstor_forum</xsl:when>
                <xsl:otherwise>identifier_unknown:<xsl:value-of select="$displayLabel"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:call-template name="appendJsonObject">
           <xsl:with-param name="key"><xsl:value-of select="$identifier_key"/></xsl:with-param>
           <xsl:with-param name="val"><xsl:value-of select="rdf:value"/></xsl:with-param>
        </xsl:call-template>
    </xsl:template>

    <!-- section: geographics -->
    <xsl:template name="damsCartographics" match="dams:Cartographics">
        <xsl:for-each select="*[not(local-name()='event')]">
            <xsl:variable name="geographic_key">
                <xsl:choose>
                    <xsl:when test="local-name() = 'point'">point</xsl:when>
                    <xsl:when test="local-name() = 'polygon'">polygon</xsl:when>
                    <xsl:when test="local-name() = 'line'">line</xsl:when>
                    <xsl:when test="local-name() = 'projection'">projection</xsl:when>
                    <xsl:when test="local-name() = 'referenceSystem'">reference_system</xsl:when>
                    <xsl:when test="local-name() = 'scale'">scale</xsl:when>
                    <xsl:otherwise>geographic_unknown:<xsl:value-of select="local-name()"/></xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:call-template name="appendJsonObject">
               <xsl:with-param name="key"><xsl:value-of select="$geographic_key"/></xsl:with-param>
               <xsl:with-param name="val"><xsl:value-of select="."/></xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:template>

    <!-- section rights: dams:License -->
    <xsl:template name="damsLicense" match="dams:License">
        <xsl:for-each select="*[not(local-name()='event')]">
            <xsl:variable name="rights_key">
                <xsl:choose>
                    <xsl:when test="local-name() = 'licenseNote'">license</xsl:when>
                    <xsl:when test="local-name() = 'licenseURI'">license</xsl:when>
                    <xsl:otherwise>rights_unknown:<xsl:value-of select="local-name()"/></xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:call-template name="appendJsonObject">
               <xsl:with-param name="key"><xsl:value-of select="$rights_key"/></xsl:with-param>
               <xsl:with-param name="val"><xsl:value-of select="."/></xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:template>

    <!-- section rights: dams:Copyright -->
    <xsl:template name="damsCopyright" match="dams:Copyright">
        <xsl:for-each select="*[not(local-name()='event')]">
            <xsl:variable name="rights_key">
                <xsl:choose>
                    <xsl:when test="local-name() = 'copyrightJurisdiction'">copyright_jurisdiction</xsl:when>
                    <xsl:when test="local-name() = 'copyrightStatus'">copyright_status</xsl:when>
                    <xsl:when test="local-name() = 'copyrightNote'">rights_note</xsl:when>
                    <xsl:when test="local-name() = 'copyrightPurposeNote'">rights_statement</xsl:when>
                    <xsl:otherwise>rights_unknown:<xsl:value-of select="local-name()"/></xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:call-template name="appendJsonObject">
               <xsl:with-param name="key"><xsl:value-of select="$rights_key"/></xsl:with-param>
               <xsl:with-param name="val"><xsl:value-of select="."/></xsl:with-param>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:template>

    <!-- section subject: mads:Subject | dams:Subject -->
    <xsl:template name="subjectElement" match="mads:Subject | dams:Subject">
        <xsl:param name="node"/>

        <xsl:variable name="node_name"><xsl:value-of select="local-name($node)"/></xsl:variable>
        <xsl:variable name="subject_key">
            <xsl:choose>
                <xsl:when test="$node_name = 'ConferenceName'">subject_name</xsl:when>
                <xsl:when test="$node_name = 'CorporateName'">subject_name</xsl:when>
                <xsl:when test="$node_name = 'Cruise'">subject_name</xsl:when>
                <xsl:when test="$node_name = 'FamilyName'">subject_name</xsl:when>
                <xsl:when test="$node_name = 'PersonalName'">subject_name</xsl:when>
                <xsl:when test="$node_name = 'CommonName'">subject_topic</xsl:when>
                <xsl:when test="$node_name = 'CulturalContext'">subject_topic</xsl:when>
                <xsl:when test="$node_name = 'GenreForm'">subject_topic</xsl:when>
                <xsl:when test="$node_name = 'Anatomy'">subject_topic</xsl:when>
                <xsl:when test="$node_name = 'Lithology'">subject_topic</xsl:when>
                <xsl:when test="$node_name = 'Occupation'">subject_topic</xsl:when>
                <xsl:when test="$node_name = 'Series'">subject_topic</xsl:when>
                <xsl:when test="$node_name = 'Topic'">subject_topic</xsl:when>
                <xsl:when test="$node_name = 'Geographic'">subject_spatial</xsl:when>
                <xsl:when test="$node_name = 'ScientificName'">subect_scientific_name</xsl:when>
                <xsl:when test="$node_name = 'Temporal'">subject_temporal</xsl:when>
                <xsl:otherwise>subject_unknown:<xsl:value-of select="$node_name"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:call-template name="appendJsonObject">
           <xsl:with-param name="key"><xsl:value-of select="$subject_key"/></xsl:with-param>
           <xsl:with-param name="val"><xsl:value-of select="$node/mads:authoritativeLabel"/></xsl:with-param>
        </xsl:call-template>
    </xsl:template>
</xsl:stylesheet>
